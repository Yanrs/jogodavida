# Jogo Da Vida
---

## Descrição
John Horton Conway inventou o jogo da vida em 1970, quando ainda era um jovem matemático na
Universidade de Cambridge, Inglaterra. Através de Martin Gardner e de sua coluna na revista
Scientific American (outubro/1970 e janeiro/1971), o jogo foi popularizado entre os programadores
do mundo inteiro e passou a ser um dos maiores consumidores de ciclos de CPU nos anos 70.

Este jogo simula a evolução de uma sociedade de organismos vivos, onde o objetivo é dada a
configuração inicial de uma população, simular o movimento desta população ao longo de várias
gerações, de acordo com certas regras para nascimento, sobrevivência e morte de cada indivíduo.

## O Tabuleiro e as Regras
O jogo da vida é um jogo que pode ser simulado numa tela de computador. Ele é jogado em
tabuleiro, idealmente infinito. Cada uma das células do tabuleiro pode estar em um de dois estados:
viva ou morta. O estado do jogo é definido pelo estado das suas células.

Neste universo bi-dimensional e discreto, o tempo flui em passos discretos. A situação do universo
em um dado instante é chamada de geração. Uma geração define como será a próxima e assim por
diante, de acordo com algumas regras, numa evolução determinística.

As regras do jogo da vida que determinam a evolução de uma geração para a sua sucessora são
simples e locais, isto é, a situação seguinte de uma célula só depende dela e de suas oito células
vizinhas.

A evolução das figuras no universo do jogo da vida lembra o crescimento de uma colônia de
bactérias. Isto pode ser utilizado como uma analogia para facilitar a memorização das regras do
jogo:

* Se a célula está viva e tem menos de dois vizinhos, ela morre de solidão. Se ela tem mais
de três vizinhos, ela morre por problemas devidos à superpopulação.
* Uma célula morta rodeada por três células vivas resultará em uma célula viva na próxima
geração.
* Uma célula viva, adjacente a duas ou três células vivas, permanece viva.

Um aspecto importante a considerar é que todas as células vão de geração a geração ao mesmo
tempo.

## Papel do jogador
Enquanto nos outros jogos o “jogador” tem participação importante no desenrolar do jogo, isto não
acontece no jogo da vida. Aqui o jogador simplesmente define uma configuração inicial e daí, mecanicamente, o jogo se desenvolve sozinho.

---
# Entrada
A entrada será através de um arquivo, composto pelos seguintes itens:
* Quantidade de linhas da matriz MxM, onde M é a sua quantidade de linhas (matriz
quadrada).
* M linhas com M caracteres em cada linha, onde cada caractere pode ser 0 ou 1 (a própria
matriz).

## Funcionamento
Seu algoritmo deve calcular as gerações seguintes, até atingir a geração solicitada através da linha de comando. Pode consultar abaixo um exemplo de execução através da linha de comando, onde após o
nome do executável, tem-se o arquivo de entrada, a quantidade de gerações para a população. 
### Exemplo de comando
`java -jar 'jogodavida.jar' mapa.txt 100`

onde `mapa.txt` é o arquivo com a matriz para o jogo e `100` o número de gerações a serem executadas
### Saída
A saída do algoritmo é um arquivo contendo a configuração da população após todas as gerações
terem ocorrido. Cada célula contém 0 ou 1, de acordo com o valor obtido na última
geração. 
Para cada execução, o grupo deve armazenar a matriz de saída em um arquivo. A saída serácomparada com a saída esperada para validar o algoritmo

---
# Grupo Responsável
Nome | RA 
--- | --- 
Yan Santos | 207498-40
Murillo Costa | 205828-02
Augusto Santos | 203690-83
Lívia Oliveira | 206056-76
Milene Mayumi | 205157-16
Pedro Zingra | 205892-03
Ana Carrer | 205417-57
Letícia Pereira | 207472-05


### Versão
0.9.2