package jogodavida.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class AppJogoDaVida {
    private int tamanho = 0;
    private int[][] mapa = null;

    private static AppJogoDaVida instancia = null;
    
    //Caso nao exista uma instancia aberta, inicia uma nova instancia.
    //"Instancia e um processo utilizado em linguagem Orientada a Objetos para criar uma copia de um certo objeto"
    public static final AppJogoDaVida getInstance() {
        if (instancia == null) {
            instancia = new AppJogoDaVida();
        }
        return instancia;
    }

    
    public void IniciaJogoDaVida(final String mapadoJogo, final int numGeracoes) throws IOException {
        iniciaSimulacao(mapadoJogo, numGeracoes);//O Parametro mapadoJogo eh o args[0] e o numGeracoes eh o args[1].
        salvarArquivo(imprimeMapa(), numGeracoes);//Salva o mapa junto ao conte�do em um arquivo txt.
        System.out.println("Finalizando Simulacao.");
    }

    public void iniciaSimulacao(final String mapadoJogo, final int numGeracoes) throws IOException {
        System.out.println("Iniciando Simulacao...");
        List<String> linhas = lerArquivoMapa(mapadoJogo);//Inicia uma lista com as linhas do mapadoJogo.
        carregarValoresCelulas(linhas);//Carrega a lista com os dados do arquivo txt.
        
        //Gera o numero de Geracoes
        for (int i = 0; i < numGeracoes; i++) {
            mapa = geracao();
            System.out.println("Geracao: " + (i + 1));
        }
    }

    public void carregarValoresCelulas(List<String> strings) {
        setTamanho(Integer.parseInt(strings.get(0)));//Altera o valor do tamanho a matriz.
        mapa = new int[tamanho][tamanho];//Inicia a matriz com seu novo tamanho.
        //Atribui os valores do txt para a matriz.
        for (int i = 1; i < tamanho+1; i++) {
            for (int j = 0; j < tamanho; j++) {
                char caracter = strings.get(i).charAt(j);//Pega na posicao o caracter no caso (0 e 1), grava em uma variavel auxiliar.

                int valor = Integer.valueOf(String.valueOf(caracter));//Converte de String para Inteiro.
                mapa[i-1][j] = valor;//Grava na matriz o valor.
            }
        }
    }
    
    private List<String> lerArquivoMapa(final String mapadoJogo) throws IOException {
        ArrayList<String> linhas = new ArrayList<>();//Declarando um arraylist para as linhas

        try (BufferedReader leitor = new BufferedReader(new FileReader(mapadoJogo))) {//Inicializa um leitor para ler cada linha do txt
            String linha = leitor.readLine();//Le a primeira linha
            
            //Enquanto a linha for diferente de nulo, continua ate gravar todas as linhas do arquivo txt
            while (linha != null) {
                boolean adc;
                adc = linhas.add(linha);
                linha = leitor.readLine();
            }
        }
        return linhas;//Retorna o arraylist de linhas
    }

    private void salvarArquivo(String conteudo, int generationNumber) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(generationNumber + "geracoes.txt");//A classe FileWriter serve para escrever diretamente no arquivo
        writer.println(conteudo);//Escreve o conteudo no novo arquivo
        writer.close();//Fecha o arquivo
    }

    public final String imprimeMapa() {
        String mapafinal = "";
        this.mapa = getMatriz();//Carrega a matriz no mapa
        for (int i = 0; i < tamanho; i++) {//Linha da matriz
            for (int j = 0; j < tamanho; j++) {//coluna da matriz
                mapafinal += Integer.toString(mapa[i][j]);//Monta o mapafinal com a matriz
            }
            mapafinal += "\n";//Pula uma linha. Obs: "+=" serve para concatenar
        }
        return mapafinal;
    }
    
    private int verificarVizinhos(int i, int j) {
        try {
            return mapa[i][j];//Retorna o mapa(matriz) com os parametros passados pelo metodo
        } catch (Exception e) {
            return 0;//Tratamento para a situacao de excecao, ele retorna 0
        }
    }

    public int vizinhos(int i, int j) {
        return verificarVizinhos(i - 1, j - 1)//inf. esquerdo
                + verificarVizinhos(i - 1, j)//inferior
                + verificarVizinhos(i - 1, j + 1)//inf. direito
                + verificarVizinhos(i, j - 1)//esquerdo
                + verificarVizinhos(i, j + 1)//direito
                + verificarVizinhos(i + 1, j - 1)//sup. esquerdo
                + verificarVizinhos(i + 1, j)//superior
                + verificarVizinhos(i + 1, j + 1);//superior direito

    }

    private int[][] geracao() {
        int aux[][] = new int[this.tamanho][this.tamanho];//Declara uma nova matriz com tamanho definido pela variavel "tamanho"
        for (int i = 0; i < tamanho; i++) {//Linha da matriz
            for (int j = 0; j < tamanho; j++) {//Coluna da matriz
            	//Aqui eh regra de neg�cio do jogo da vida, eh bem simples de entender. Caso alguem tenha alguma duvida, me comunicar.
                if (mapa[i][j] == 1) {
                    if (vizinhos(i, j) < 2 || vizinhos(i, j) > 3) {
                        aux[i][j] = 0;
                    } else {
                        aux[i][j] = 1;
                    }
                } else {
                    if (vizinhos(i, j) == 3) {
                        aux[i][j] = 1;
                    } else {
                        aux[i][j] = 0;
                    }
                }
            }
        }
        return aux;
    }
    
    //Permite que voce chame o Mapa (Matriz)
    public int[][] getMatriz() {
        return mapa;
    }
    
    //Permite voce alterar o tamanho da matriz, apenas sendo necessario chamar set e atribui um novo valor
    public void setTamanho(int tamanho) {
        this.tamanho=tamanho;
    }
}
