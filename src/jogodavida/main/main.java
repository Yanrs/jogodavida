package jogodavida.main;

import java.io.IOException;

public class main {
    public static void main(String[] args) throws IOException {
            if (args.length < 2) {
                String mensagem = "A aplicacao precisa de dois argumentos: 'mapadoJogo' e 'numGeracoes'.\n";
                mensagem += "'mapadoJogo' um arquivo txt contendo linhas de 0's e 1's.\n "
                        + "Cada linha deve conter a mesma quantidade de caracteres.\n"
                        + "'numGeracoes': O numero de gerações que devem ser executadas.\n"
                        + "Exemplo: java -jar \"jogodavida.jar\" mapa.txt 100\n";
                System.out.println(mensagem);
            } else {
                AppJogoDaVida.getInstance().IniciaJogoDaVida(args[0], Integer.valueOf(args[1]));
            }
        
    }
}
